Bootstrap Grid
==================================

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Functionality
 * Modules
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting & FAQ
 * Maintainers

INTRODUCTION
------------
Bootstrap Grid is Views extension to provide Bootstrap 3 Grid compatible
responsive grid with enhanced UI.

FUNCTIONALITY
------------
Responsive Views grid, adjustable for every major resolutions

* Large Screens - desktops
* Medium Screens - notebooks
* Small Screens - tablets
* Extra Small Screens - Mobile


REQUIREMENTS
------------
This module requires having those following modules:
 * Views (https://drupal.org/project/views)
 * Chaos tool suite (ctools) (https://drupal.org/project/ctools)
 * To ensure correct functionality, module should be always used with Bootstrap3 supported theme!


INSTALLATION
------------
Recommended way of installing is by using drush (https://drupal.org/node/1791676)
Commands that needs to be executed:

drush dl bootstrap_grid
drush en bootstrap_grid

CONFIGURATION
-------------

Besides configuring Views with Bootstrap Grid display, no additional configuration is required.
